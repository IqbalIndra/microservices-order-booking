package com.microservices.gojek.orderbookservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
/*import org.springframework.cloud.netflix.eureka.EnableEurekaClient*/

@SpringBootApplication
/*@EnableEurekaClient*/
class OrderBookServiceApplication

fun main(args: Array<String>) {
    runApplication<OrderBookServiceApplication>(*args)
}
