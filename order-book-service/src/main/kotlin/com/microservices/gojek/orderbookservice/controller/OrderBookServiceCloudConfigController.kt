package com.microservices.gojek.orderbookservice.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RefreshScope
class OrderBookServiceCloudConfigController
{
    @Value("\${order-book-service.testappname}")
    val testappname : String? = null

    @GetMapping("/test")
    fun getTestCloudConfig(): String? = testappname
}