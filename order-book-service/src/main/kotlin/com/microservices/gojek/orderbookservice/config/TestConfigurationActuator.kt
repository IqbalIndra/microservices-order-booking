package com.microservices.gojek.orderbookservice.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "order-book-service")
class TestConfigurationActuator(val appname : String? ="")