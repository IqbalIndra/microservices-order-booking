package com.microservices.gojek.midtranspaymentservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@SpringBootApplication
@EnableEurekaClient
class MidtransPaymentServiceApplication

fun main(args: Array<String>) {
    runApplication<MidtransPaymentServiceApplication>(*args)
}
